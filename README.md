# interview_problems

Playgrounds
-----------
- https://jsfiddle.net - JavaScript
- https://dotnetfiddle.net - .NET/C# 
- https://play.golang.org - Go
- https://leetcode.com/playground/new/empty - C++, Java, Python, Python3, C, C#, JavaScript, TypeScript, Ruby, Swift, Go, Bash, Scala, Kotlin, Rust, PHP

You may also use your local development environment.

Problem #1 
----------
Build a paging function to limit the number of items in an array to only return the items that would appear on the requested page. For example, given an array of 20 items, a page size of 10, and a page number of 1, the function would return the first 10 items.

The function signature would be something like
```
function limitItems(items: any[], pageSize: number, pageNumber: number): any[] 
```

Problem #2
----------
Build a function to return list of devices from a device database based on the current user authorisation level.

- Device database contains set of available devices in the organisation.
- A patient is a person account record having association with a clinic, devices, etc.
- Patient and doctors are associated with atleast one clinic. (can be multiple clinics)

- Each device has only 1 patient association and has a mandatory field named as `owner`.
- A user is authorised to view only related device associated with themself or associated with devices in their clinic. 
- In addition, patient has given consent only to specific clinics to view their data. So make sure return data if clinic has consent from patient. If not, show message "You are not authorised to view data".

For example, 
1. if a patient logins, then function should only return the associated devices.
1. if a doctor logins, then function should only return all the devices of patients associated in their clinic.

- The function should take in the current user and a set of devices. The authorisation function should filter the equipment argument based on the authorisation rules, returning only the devices the user is authorised to view.
- User has a user type attribute which defines the logged in userType. 
- The function(s) can use collections to mock the data (optional)

```
| Key             | Value                          |
|-----------------|----------------------------------|
| UserType        |Patient, Doctor            |
```

The function signature would be something like
```
function authoriseddevices(currentUser: user, devices: devices[]) device[]
```

